﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horoscope.Member_One.BusinessLogic
{
    class Date
    {
        //field variables used for the day, month, and year 
        private int _day;
        private string _month;
        private int _year;

        //setting parameters for the variables
        public Date(int day = 1, string month = "1", int year = 2000)
        {
            _day = day;
            _month = month;
            _year = year;
        }

        //Property for Day that allows it to be accessed
        public int Day
        {
            get { return _day; }
            set { _day = value; }
        }

        //Property for month that will allow it to be accessed
        public string Month
        {
            get { return _month; }
            set { _month = value; }
        }

        //Property for year that allows it to be accessed
        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }
    }
}
