﻿using Horoscope.Member_One.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

namespace Horoscope.Member_One
{
    class Horoscopes
    {
        //Create the array that will contain all 12zodiac signs
        private ZodiacSign[] _zodiacSignArray;

        //Create zodiac sign for user
        private ZodiacSign _userZodiacSign;

        //create new array with the first set of horoscope predictions
        private string[] _predictionSetOne = new string[] {"Sad", "Happy", "Angry", "Excited", "Awkward", "Frustrated" };

        //create new array that contains the second set of predictions
        private string[] _predictionSetTwo = new string[] {"Shocked", "Stunned", "Suprised", "Contempt", "Positive", "Negative" };

        //List of the predictions
        private List<string> _prediction = new List<string>();

        //Generate a random number
        Random _random = new Random();

        //Create a variable that will contain the generated random number
        //It will be used to make the predictions random when inputting the date to keep things "fresh"
        private int _randomNum = 0;

        //Create list where the random predictions will be contained
        private List<string> _randomPredictions = new List<string>();

        //DONE: Create a path that can save the files to a specified folder
        string _dataDirectoryPathHoro = Path.Combine(ApplicationData.Current.LocalFolder.Path, "Fortunes.csv");

        //Property that allows access to the user zodiac sign
        public ZodiacSign UserZodiacSign
        {
            get { return _userZodiacSign; }
            set { _userZodiacSign = value; }
        }

        public Horoscopes(Date date)
        {
            //DONE: Create the objects that will be used for the zodiac signs
            ZodiacSign _aries = new ZodiacSign("Aries", new BitmapImage(new Uri("ms-appx:///Assets/aries.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _taurus = new ZodiacSign("Taurus", new BitmapImage(new Uri("ms-appx:///Assets/taurus.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _gemini = new ZodiacSign("Gemini", new BitmapImage(new Uri("ms-appx:///Assets/gemini.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _cancer = new ZodiacSign("Cancer", new BitmapImage(new Uri("ms-appx:///Assets/cancer.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _capricorn = new ZodiacSign("Capricorn", new BitmapImage(new Uri("ms-appx:///Assets/capricorn.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _leo = new ZodiacSign("Leo", new BitmapImage(new Uri("ms-appx:///Assets/leo.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _libra = new ZodiacSign("Libra", new BitmapImage(new Uri("ms-appx:///Assets/libra.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _pisces = new ZodiacSign("Pisces", new BitmapImage(new Uri("ms-appx:///Assets/pisces.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _sagittarius = new ZodiacSign("Sagittarius", new BitmapImage(new Uri("ms-appx:///Assets/sagittarius.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _scorpio = new ZodiacSign("Scorpio", new BitmapImage(new Uri("ms-appx:///Assets/scorpio.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _virgo = new ZodiacSign("Virgo", new BitmapImage(new Uri("ms-appx:///Assets/virgo.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);
            ZodiacSign _aquarius = new ZodiacSign("Aquarius", new BitmapImage(new Uri("ms-appx:///Assets/aquarius.png")), _randomPredictions[0], _randomPredictions[1], _randomPredictions[3]);

            //Zodiacsplaced in array after objects created in the order of how they start from january to decemberfor simplicity and easy use in the case portion of the class
            _zodiacSignArray = new ZodiacSign[] { _aquarius, _pisces, _aries, _taurus, _gemini, _cancer, _leo, _capricorn, _virgo, _libra, _scorpio, _sagittarius, _capricorn };


            //gets the correct zodiacaccording to whatever the date is
            DetermineZodiacSign(date);

            //Saves the zodiac name and whatever the predictionwas alongside it
            Save();

            //DONE: Create the predictions by taking an emotion from the first array and then taking a reaction from the 2nd array
            //then form a statement combining the two items.
            foreach (string emotion in _predictionSetOne)
            {
                foreach (string reaction in _predictionSetTwo)
                {
                    _prediction.Add($"{emotion} and {reaction}");
                }
            }

            //Done: Randomize the predictions
            for (int randomPrediction = 1; randomPrediction <= 36; randomPrediction++)
            {
                //gets the random number between the specified range
                _randomNum = _random.Next(0, _prediction.Count - 1);
                //_randomNum = _random.Next(0, _prediction.Count);

                //adds the prediction to the list of random predictions
                _randomPredictions.Add(_prediction[_randomNum]);

                //removes the prediction so that it wont be used again from the list
                _prediction.RemoveAt(_randomNum);
            }


            //TODO: 
        }

        //Done: Use cases to determine which zodiac sign will be used according to the date that was entered
        public ZodiacSign DetermineZodiacSign(Date date)
        {
            //Determines which month and then follows suit in between the specified days to use correct zodiac sign
            switch(date.Month)
            {
                //january
                case "1":
                    if (1 <= date.Day & date.Day <= 19)
                    {
                        _userZodiacSign = _zodiacSignArray[11];
                    }
                    else if (20 <= date.Day & date.Day <= 31)
                    {
                        _userZodiacSign = _zodiacSignArray[0];
                    }
                    break;

                    //february
                case "2":
                    if (1 <= date.Day & date.Day <= 17)
                    {
                        _userZodiacSign = _zodiacSignArray[0];
                    }
                    //29 days forfebruary in case of Leap Year
                    else if (18 <= date.Day & date.Day <= 29)
                    {
                        _userZodiacSign = _zodiacSignArray[1];
                    }
                    break;

                    //March
                case "3":
                    if (1 <= date.Day & date.Day <= 20)
                    {
                        _userZodiacSign = _zodiacSignArray[1];
                    }
                    else if (21 <= date.Day & date.Day <= 31)
                    {
                        _userZodiacSign = _zodiacSignArray[2];
                    }
                    break;


                    //April
                case "4":
                    if (1 <= date.Day & date.Day <= 19)
                    {
                        _userZodiacSign = _zodiacSignArray[2];
                    }
                    else if (20 <= date.Day & date.Day <= 30)
                    {
                        _userZodiacSign = _zodiacSignArray[3];
                    }
                    break;


                    //may
                case "5":
                    if (1 <= date.Day & date.Day <= 20)
                    {
                        _userZodiacSign = _zodiacSignArray[3];
                    }
                    else if (21 <= date.Day & date.Day <= 31)
                    {
                        _userZodiacSign = _zodiacSignArray[4];
                    }
                    break;


                    //june
                case "6":
                    if (1 <= date.Day & date.Day <= 20)
                    {
                        _userZodiacSign = _zodiacSignArray[4];
                    }
                    else if (21 <= date.Day & date.Day <= 30)
                    {
                        _userZodiacSign = _zodiacSignArray[5];
                    }
                    break;


                    //July
                case "7":
                    if (1 <= date.Day & date.Day <= 22)
                    {
                        _userZodiacSign = _zodiacSignArray[5];
                    }
                    else if (23 <= date.Day & date.Day <= 31)
                    {
                        _userZodiacSign = _zodiacSignArray[6];
                    }
                    break;


                    //August
                case "8":
                    if (1 <= date.Day & date.Day <= 23)
                    {
                        _userZodiacSign = _zodiacSignArray[6];
                    }
                    else if (24 <= date.Day & date.Day <= 31)
                    {
                        _userZodiacSign = _zodiacSignArray[7];
                    }
                    break;


                    //September
                case "9":
                    if (1 <= date.Day & date.Day <= 22)
                    {
                        _userZodiacSign = _zodiacSignArray[7];
                    }
                    else if (23 <= date.Day & date.Day <= 30)
                    {
                        _userZodiacSign = _zodiacSignArray[8];
                    }
                    break;


                    //October
                case "10":
                    if (1 <= date.Day & date.Day <= 22)
                    {
                        _userZodiacSign = _zodiacSignArray[8];
                    }
                    else if (23 <= date.Day & date.Day <= 31)
                    {
                        _userZodiacSign = _zodiacSignArray[9];
                    }
                    break;

                    //November
                case "11":
                    if (1 <= date.Day & date.Day <= 22)
                    {
                        _userZodiacSign = _zodiacSignArray[9];
                    }
                    else if (23 <= date.Day & date.Day <= 30)
                    {
                        _userZodiacSign = _zodiacSignArray[10];
                    }
                    break;

                    //December
                case "12":
                    if (1 <= date.Day & date.Day <= 22)
                    {
                        _userZodiacSign = _zodiacSignArray[10];
                    }
                    else if (23 <= date.Day & date.Day <= 31)
                    {
                        _userZodiacSign = _zodiacSignArray[11];
                    }
                    break;
            }
            //return appropriate zodiac sign 
            return _userZodiacSign;
        }


        //Done: Create a save method that will save all of the predctions done up that point
        //usedStream Writer learned from last semester to do this portion
        private void Save()
        {
            //Use stream reader to save data into a file
            using (StreamWriter writer = new StreamWriter(new FileStream(_dataDirectoryPathHoro, FileMode.Create)))
            {
                //Use a foreach loop to go through the zodiacs in the array
                foreach (ZodiacSign zodiac in _zodiacSignArray)
                {
                    //Writes what the name of the zodiac is
                    writer.Write($"{zodiac.ZodiacName}: ");

                    //Another foreach loop o go through all of the predictions
                    foreach (string prediction in zodiac.ZodiacPredictions)
                    {
                        //Then writes what the prediction was next to the zodiac sign that was chosen
                        writer.Write($"{prediction}----> ");
                    }
                    //Space for next prediction
                    writer.WriteLine("");
                }
            }
        }
    }
}
