﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace Horoscope.Member_One
{
    class ZodiacSign
    {
        private string _zodiacName;

        private string[] _zodiacPredictions = new string[3];

        private Image _imageOfSign;

        //TODO: Get the basis of how the zodiac objects will be created
        //Will contain what thefields specify, the image, name, and predictions
        public ZodiacSign(string nameOfZodiac, BitmapImage zodiacSignImage, string predictionOne, string predictionTwo, string predictionThree)
        {
            //The image of the zodiac sign itself
            _imageOfSign = new Image();

            //uses source in order to get the image
            _imageOfSign.Source = zodiacSignImage;

            //this is the name of the zodiac sign
            _zodiacName = nameOfZodiac;

            //will have THREE predictions made
            _zodiacPredictions = new string[] { predictionOne, predictionTwo, predictionThree };
        }







        //////////////////////////////////////////////////////////////////////////////////////
        //Properties that provide access to the fields.
        //////////////////////////////////////////////////////////////////////////////////////

        public string ZodiacName
        {
            get { return _zodiacName; }
            set { _zodiacName = value; }
        }

        public string[] ZodiacPredictions
        {
            get { return _zodiacPredictions; }
            set { _zodiacPredictions = value; }
        }

        public Image ImageOfSign
        {
            get { return _imageOfSign; }
            set { _imageOfSign = value; }
        }
    }
}
