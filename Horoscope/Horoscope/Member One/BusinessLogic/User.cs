﻿using Horoscope.Member_One.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Horoscope.Member_One
{
    class User
    {
        //field variables that will be used for this class involving birthday, zodiac sign, username
        private string _userName;

        private Date _birthday;

        ZodiacSign _zodiacSign;

        //sets default name and checks whether or no ta birthday has been entered and creates a dateif one hasnt
        //otherwise the birthday entered will be the new birthday
        public User(Date birthday, string userName = "Arnold Schwarzenegger")
        {
            _userName = userName;

            if (birthday != null)
            {
                _birthday = birthday;
            }
            else if (birthday == null)
            {
                _birthday = new Date();
            }
        }

        //Property for Username that allows it to be accessed
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        //Birthday property that allows it to be accessed
        public Date Birthday
        {
            get { return _birthday; }
            set { _birthday = value; }
        }

        //ZodiacSign property that allows itto be accessed
        public ZodiacSign ZodiacSign
        {
            get { return _zodiacSign; }
            set { _zodiacSign = value; }
        }
    }
}
