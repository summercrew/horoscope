﻿using Horoscope.Presentation;
using Horoscope.Member_One.BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;
using Horoscope.Member_One;
using Windows.Storage;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Horoscope
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //Variable for User storgae
        private User _user;

        //Variable for date storage
        private Date _date = new Date();

        //Variable for storage of horoscope
        private Horoscopes _horoscope;

        //generate random number
        Random _randomNum = new Random();

        //NavCon storage
        NavCon _navcon = new NavCon();

        //required data directory path so that files or info can be saved
        private string _dataDirectoryPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "UserData.txt");



        public MainPage()
        {
           
            this.InitializeComponent();
            

        }

        private void OnNavigateBack(object sender, BackRequestedEventArgs e)
        {
            if (_frmContent.CanGoBack)
            {
                _frmContent.GoBack();
                e.Handled = true;
            }
        }


        private void OnNavigationItemClicked(object sender, ItemClickEventArgs e)
        {
            NavMenuItem navMenuItem = e.ClickedItem as NavMenuItem;
            

        }

        private void OnContentFrameNavigated(object sender, NavigationEventArgs e)
        {
            

        }

        private void OnChangeSerializer(object sender, RoutedEventArgs e)
        {
            //initialize the concrete serializer to be used depending
            //on which radio button was checked
            //if (sender == _rbtnTextSerializer)
            //{
            //    _accountSerializer = new AccountTextSerializer();
            //}
            //else if (sender == _rbtnXmlSerializer)
            //{
            //    _accountSerializer = new AccountXmlSerializer();
            //}
            //else if (sender == _rbtnJsonSerializer)
            //{
            //    _accountSerializer = new AccountJsonSerializer();
            //}
            //else
            //{
            //    Debug.Assert(false, "Unknown file format. Cannot create serializer");
            //}
        }

      


        private void OnApply(object sender, RoutedEventArgs e)
        {

        }
    }
}
